/* Include guard ---------------------------------- */
#ifndef BSP_STARTUP_H
#define BSP_STARTUP_H

/* Includes ---------------------------------------- */
#include "BSP_systick.h"
#include "BSP_CAN.h"
#include "BSP_USART.h"

#include "stdint.h"

extern uint8_t calibrating_done;
extern uint8_t fault_check;

void BSP_Startup_startup_routine(void);
void BSP_Startup_check_errors(void);

#endif //BSP_STARTUP_H
