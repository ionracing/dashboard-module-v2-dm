/**************************************************
*   ADC                                           *
*                                                 *
*   Configures the ADC channels                   *
*   for the sensor module.                        *
*                                                 *
*   TODO: Add oversampling.                       *
*   Board specific?                               *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_GPIO.h"
#include "BSP_ADC.h"

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_adc.h"

/* Defines --------------------------------------------------- */

/**
* @Adefgroup ADC: Define the channels to be used. Comment out channels that
									are unused.
*/
#define ADC_GPIO = GPIOA
#define ADC_CH0 ADC_Channel_0
#define ADC_CH1 ADC_Channel_1
#define ADC_CH2 ADC_Channel_2
#define ADC_CH3 ADC_Channel_3
#define ADC_CH4 ADC_Channel_4
#define ADC_CH5 ADC_Channel_5
#define ADC_CH6 ADC_Channel_6
#define ADC_CH7 ADC_Channel_7

#define ADC_ALL_CHANNELS (ADC_CH0 | ADC_CH1 | ADC_CH2 | ADC_CH3 | ADC_CH4 | ADC_CH5 | ADC_CH6)
#define ADC_SAMPLING_FREQUENCY 1000 //Hz

/**
* @defgroup DMA
*/
/* Local function prototypes ----------------------------------*/

/* Variables --------------------------------------------------*/

/* Init functions -------------------------------------------- */

/* Single channel configuration. */
void BSP_ADC_SIMPLE_init(void)
{	
	ADC_InitTypeDef	ADC_initStruct;
	ADC_CommonInitTypeDef ADC_commonInitStruct;
	GPIO_InitTypeDef GPIO_initStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	ADC_StructInit(&ADC_initStruct);
	ADC_CommonStructInit(&ADC_commonInitStruct);
	GPIO_StructInit(&GPIO_initStruct);
	
	/* Configurate GPIO */
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_initStruct);
	
	/* Configure ADC in independent mode */
	ADC_commonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_commonInitStruct.ADC_Mode = ADC_Mode_Independent;
	ADC_commonInitStruct.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);
	
	/* Configure the ADC: 12-bit single conversion */
	ADC_initStruct.ADC_Resolution = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode	= DISABLE;
	ADC_initStruct.ADC_ContinuousConvMode = DISABLE;
	ADC_initStruct.ADC_ExternalTrigConvEdge = 0;
	ADC_initStruct.ADC_ExternalTrigConv = 0;
	ADC_initStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_initStruct);
	
	ADC_Cmd(ADC1, ENABLE);
}

uint16_t BSP_ADC_SIMPLE_readChannel(void)
{
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_480Cycles);
	ADC_SoftwareStartConv(ADC1);
	while((ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET));
	return ADC_GetConversionValue(ADC1);
}
