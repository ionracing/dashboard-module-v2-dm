/* Include guard ---------------------------------- */
#ifndef BSP_I2C_H
#define BSP_I2C_H

/* Includes ---------------------------------------- */
#include "BSP_GPIO.h"

#include "stm32f4xx_i2c.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void BSP_I2C_Init
(
    I2C_TypeDef* I2Cx, 
    uint16_t Ack, 
    uint16_t AcknowledgedAdress, 
    uint32_t ClockSpeed, 
    uint16_t DutyCycle, 
    uint16_t Mode, 
    uint16_t OwnAddress1
);
void BSP_I2C_GPIO_Init
(
	I2C_TypeDef* I2Cx
);
void BSP_I2C_RCC_DeInit
(
	I2C_TypeDef* I2Cx
);
void BSP_I2C_RCC_Init
(
	I2C_TypeDef* I2Cx
);
void BSP_I2C_Write_8
(
	I2C_TypeDef* I2Cx,
	uint8_t addr,
	uint8_t reg,
	uint8_t data
);
void BSP_I2C_Write_16
(
	I2C_TypeDef* I2Cx,
	uint8_t addr,
	uint8_t reg,
	uint16_t data
);
uint8_t BSP_I2C_Read_8
(
	I2C_TypeDef* I2Cx,
	uint8_t addr,
	uint8_t reg
);
uint16_t BSP_I2C_Read_16
(
	I2C_TypeDef* I2Cx,
	uint8_t addr,
	uint8_t reg_MSB,
	uint8_t reg_LSB
);
uint8_t BSP_I2C_Check
(
    I2C_TypeDef* I2Cx,
    uint8_t addr,
    uint8_t reg,
    uint8_t check
);

#endif //BSP_I2C_H
