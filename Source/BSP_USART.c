/**************************************************
*   USART                                         *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_USART.h"

/*midlertidig defines for at koden skal kompilere*/
#define RPI_DATA 0x000
#define CAN_RPI_DATA 0x001
uint8_t cal_break_data;

volatile uint16_t GPS_New_Data = 0;

char USARTData[100];
volatile int p = 0;

/**
 * @brief Initialising U(S)ARTx.
 *
 * @param USARTx/UARTx				x = 1, 2, 3, 4, 5, 6.
 * @param BaudRate					example 9600.
 * @param WordLength				USART_WordLength_x => x = 8b, 9b.
 * @param StopBits					USART_StopBits_x => x = 0_5, 1, 1_5, 2.
 * @param Parity					USART_Parity_x => x = Even, No, Odd.
 * @param Mode						USART_Mode_x => x = Rx, Tx.
 * @param HardwareFlowControl		USART_HardwareFlowControl_x => x = CTS, None, RTS, RTS_CTS.
 *
 */
void BSP_USART_Init(USART_TypeDef* USARTx, uint32_t BaudRate, uint16_t WordLength, uint16_t StopBits, uint16_t Parity, uint16_t Mode, uint16_t HardwareFlowControl)
{
	USART_InitTypeDef USART_InitStruct;
	
	BSP_USART_RCC_Init(USARTx);

	BSP_USART_GPIO_Init(USARTx);

	USART_InitStruct.USART_BaudRate = BaudRate;
	USART_InitStruct.USART_WordLength = WordLength;
	USART_InitStruct.USART_StopBits = StopBits;
	USART_InitStruct.USART_Parity = Parity;
	USART_InitStruct.USART_Mode = Mode;
	USART_InitStruct.USART_HardwareFlowControl = HardwareFlowControl;
    USART_Init(USARTx, &USART_InitStruct);
    
    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

	USART_Cmd(USARTx, ENABLE);
}

/**
 * @brief Initialise USARTx GPIO.
 *
 * @param USARTx		x = 1, 2, 3, 4, 5, 6.
 *
 */
void BSP_USART_GPIO_Init(USART_TypeDef* USARTx)
{
	if(USARTx == USART1)//THIS FUCKER DOES NOT WORK ON THE STM32F4xx Developer board.
	{
		//Pin9 - Tx, Pin10 - Rx.
		BSP_GPIO_Init(GPIOA, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_9 | GPIO_Pin_10, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1); //Rx -> Tx.
	}
	else if(USARTx == USART2)
	{
		//Pin2 - Tx, Pin3 - Rx.
		BSP_GPIO_Init(GPIOA, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_2 | GPIO_Pin_3, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2); //Rx -> Tx.
	}
	else if(USARTx == USART3)
	{
		//Pin10 - Tx, Pin11 - Rx.
		BSP_GPIO_Init(GPIOB, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_10 | GPIO_Pin_11, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3); //Rx -> Tx.
	}
	else if(USARTx == UART4)
	{
		//Pin0 - Tx, Pin1 - Rx.
		BSP_GPIO_Init(GPIOA, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_0 | GPIO_Pin_1, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4); //Rx -> Tx.
	}
	else if(USARTx == UART5)
	{
		//Pin12 - Tx, Pin2 - Rx.
		BSP_GPIO_Init(GPIOC, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_12, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		BSP_GPIO_Init(GPIOD, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_2, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_UART5); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5); //Rx -> Tx.
	}
	else if(USARTx == USART6)
	{
		//Pin6 - Tx, Pin7 - Rx.
		BSP_GPIO_Init(GPIOC, GPIO_Mode_AF, GPIO_OType_PP, GPIO_Pin_6 | GPIO_Pin_7, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6); //Tx -> Rx.
		GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6); //Rx -> Tx.
	}
	else
		while(1);
}

/**
 * @brief Initialise USARTx RCC.
 *
 * @param USARTx		x = 1, 2, 3, 4, 5, 6.
 *
 */
void BSP_USART_RCC_Init(USART_TypeDef* USARTx)
{
	if(USARTx == USART1)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	else if(USARTx == USART2)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	else if(USARTx == USART3)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	else if(USARTx == UART4)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
	else if(USARTx == UART5)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
	else if(USARTx == USART6)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	else
		while(1);
}

/**
 * @brief Initialise USARTx NVIC.
 *
 * @param USARTx		x = 1, 2, 3, 4, 5, 6.
 *
 */
void BSP_USART_NVIC_Init(USART_TypeDef* USARTx)
{
	NVIC_InitTypeDef NVIC_InitStruct;
    
	if(USARTx == USART1)
		NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;
	else if(USARTx == USART2)
		NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
	else if(USARTx == USART3)
		NVIC_InitStruct.NVIC_IRQChannel = USART3_IRQn;
	else if(USARTx == UART4)
		NVIC_InitStruct.NVIC_IRQChannel = UART4_IRQn;
	else if(USARTx == UART5)
		NVIC_InitStruct.NVIC_IRQChannel = UART5_IRQn;
	else if(USARTx == USART6)
		NVIC_InitStruct.NVIC_IRQChannel = USART6_IRQn;
	else
		while(1);

	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
}

/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_USART_Write_8(USART_TypeDef* USARTx, uint16_t Data)
{
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
    USART_ClearFlag(USARTx, USART_FLAG_TC);
	USART_SendData(USARTx, Data);
}

/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_USART_Write_16(USART_TypeDef* USARTx, uint16_t Data)
{
    BSP_USART_Write_8(USARTx, Data & 0xFF); //LSB
    BSP_USART_Write_8(USARTx, (Data >> 8) & 0xFF); //MSB
}

/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_USART_Write_String(USART_TypeDef* USARTx, char *chr)
{
    while(*chr != '\n')
    {
        BSP_USART_Write_8(USARTx, *chr);
        chr++;
    }
    BSP_USART_Write_8(USARTx, '\n');
}

/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_USART_Read_RPI(uint8_t address, uint8_t data1, uint8_t data2)
{
	if(address == RPI_DATA)
    {
		uint8_t data[8];
		data[0] = data2;
		
		BSP_CAN_Write_8(CAN1, CAN_RPI_DATA, 1, data);
	}
}

/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_RPI_Send(USART_TypeDef* USARTx, uint8_t address, uint8_t data1, uint8_t data2)
{
	BSP_USART_Write_8(USARTx, 0xFF);
	BSP_USART_Write_8(USARTx, 0xFF);
	BSP_USART_Write_8(USARTx, address);
	BSP_USART_Write_8(USARTx, data2);
	BSP_USART_Write_8(USARTx, data1);
}

uint8_t d;
/**
  * @brief 
  * @param 
  * @retval
  */
void BSP_USART_Read(USART_TypeDef* USARTx) // this is not done, not sure if we need it.
{
    uint8_t d = USART_ReceiveData(USARTx);
    /*if(d != '\n')
    {
        
    }
    else
    {
        //send to switch case function.
    }*/
	USARTData[p++] = d;
    p % 2 == 0 ? GPIO_SetBits(GPIOD, GPIO_Pin_13) : GPIO_ResetBits(GPIOD, GPIO_Pin_13);
    if(d == '\n')
    {
        p = 0;
        GPS_New_Data = 1;
    }
	if(p == 100)
    {
		p = 0;
	}
}
