/**************************************************
*   GPS                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_GPS.h"


USART_TypeDef* USARTx = USART2;

/**
 * We are using Adafruit Ultimate GPS Breakout that you can find here.
 * https://www.adafruit.com/products/746
 *
 * We are using USART2 and are connected like this.
 * Gps	-	stm32f4xx
 * 3.3V	- 	Vdd
 * Gnd 	- 	Gnd
 * RX	-	PA2
 * TX 	- 	PA3
 *
 * We use 3.3V instead of 5V.
 *
 */

int g = 0;
uint8_t RxByte1 = 0;
uint8_t ION_GPS_Init()
{
	BSP_USART_Init(USARTx, 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx, USART_HardwareFlowControl_None);
   
    for(g = 0; g < 0xFFFFF; g++) ;
    
    //BSP_USART_Write_String(USARTx, PMTK_SET_NMEA_OUTPUT_RMCONLY);
    if(!ION_GPS_Check(PMTK_SET_NMEA_OUTPUT_RMCONLY)) 
        return 0;
    
    //BSP_USART_Write_String(USARTx, PMTK_SET_NMEA_UPDATE_10HZ);
    if(!ION_GPS_Check(PMTK_SET_NMEA_UPDATE_10HZ))
        return 0;
    
    //BSP_USART_Write_String(USARTx, PMTK_API_SET_FIX_CTL_5HZ);
    if(!ION_GPS_Check(PMTK_API_SET_FIX_CTL_5HZ))
        return 0;
    
    BSP_USART_NVIC_Init(USARTx);
    return 1;
}

char gps_data[100];
char ACK[] = "$PMTK";
char ID[6] = "";
uint8_t ION_GPS_Success(uint8_t turns)
{    
    int i = 0;
    while(turns--)
    {
        i = 0;
        do
        {
            while(USART_GetFlagStatus(USARTx, USART_FLAG_RXNE) != SET);
            USART_ClearFlag(USARTx, USART_FLAG_RXNE);
            gps_data[i] = USART_ReceiveData(USARTx);
            i++;
        }
        while(gps_data[i-1] != 0x0A); // checking for \n.
        
        strncpy(ID, gps_data, 5);
        if(strcmp(ID, ACK) == 0)
        {
            if(gps_data[13] == '3') // checking Ack Flag. 3 eq success, else fail.
                return 1;
            else
                return 0;
        }
    }
    return  0;
}

uint8_t ION_GPS_Check(char *chr)
{
    int tries = 0;
    do
    {
        if(tries >= 2)
            return 0;
        BSP_USART_Write_String(USARTx, chr);
        tries++;
    }
    while(!ION_GPS_Success(5));
    return 1;
    /*if(ION_GPS_Success(5))
        return 1; //Success
    else
    {
        //try agian
        BSP_USART_Write_String(USARTx, chr); // not sure if this will work, sending a pointer to a pointer input.
        if(ION_GPS_Success(5))
            return 1; //Success
        else
            return 0; //Fail
    }*/
}
