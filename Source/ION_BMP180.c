/**************************************************
*   BMP180                                        *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_BMP180.h"

I2C_TypeDef* BMP180_I2C = I2C2;

int time = 0;
//short
int16_t AC1 = 408, AC2 = -72, AC3 = -14383, B1 = 6190, B2 = 4, MB = -32767, MC = -8711, MD = 2868;
//unsigned short
uint16_t  AC4 = 32741, AC5 = 32757, AC6 = 23153;
//long
int32_t  UT = 0, UP = 0, X1 = 0, X2 = 0, B5 = 0, T = 0, B6 = 0, X3 = 0, B3 = 0, P = 0;
//unsigned long
uint32_t B4 = 0, B7 = 0;

/**
 * @brief Initialise for the BMP180.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t _ION_BMP180_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    BMP180_I2C = I2Cx;
    
    //Power ON
	BSP_I2C_Write_8(BMP180_I2C, BMP180_ADDRESS, BMP180_CAL_AC1_LSB, 0x00);
    if(!BSP_I2C_Check(BMP180_I2C, BMP180_ADDRESS, BMP180_CAL_AC1_LSB, 0x00))
        return 0;
    
    return 1;
}

/**
 * @brief Initialise for the BMP180.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t ION_BMP180_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    BMP180_I2C = I2Cx;
	BSP_I2C_Init(BMP180_I2C, I2C_Ack_Enable,I2C_AcknowledgedAddress_7bit, 100000, I2C_DutyCycle_2, I2C_Mode_I2C, 0x00);

    //Power ON
	BSP_I2C_Write_8(BMP180_I2C, BMP180_ADDRESS, BMP180_CAL_AC1_LSB, 0x00);
    if(!BSP_I2C_Check(BMP180_I2C, BMP180_ADDRESS, BMP180_CAL_AC1_LSB, 0x00))
        return 0;
    
    return 1;
}

void ION_BMP180_CalibrationData()
{
	AC1 = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC1_MSB,BMP180_CAL_AC1_LSB);
	AC2 = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC2_MSB,BMP180_CAL_AC2_LSB);
	AC3 = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC3_MSB,BMP180_CAL_AC3_LSB);
	AC4 = BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC4_MSB,BMP180_CAL_AC4_LSB);
	AC5 = BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC5_MSB,BMP180_CAL_AC5_LSB);
	AC6 = BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_AC6_MSB,BMP180_CAL_AC6_LSB);
	B1 = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_B1_MSB,BMP180_CAL_B1_LSB);
	B2 = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_B2_MSB,BMP180_CAL_B2_LSB);
	MB = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_MB_MSB,BMP180_CAL_MB_LSB);
	MC = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_MC_MSB,BMP180_CAL_MC_LSB);
	MD = (int16_t)BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_CAL_MD_MSB,BMP180_CAL_MD_LSB);
}

int32_t ION_BMP180_Temperature(bmp180Oss_t oss)
{
	BSP_I2C_Write_8(BMP180_I2C,BMP180_ADDRESS,BMP180_CONTROL,BMP180_READTEMPCMD);
	for(time = 0;time < 0xFFFF;time++); // wait 4.5ms
	UT = BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_TEMPDATA_MSB,BMP180_TEMPDATA_LSB);
    
	X1 = ((UT - AC6) * AC5) / 32768;
	X2 = (MC * 2048) / (X1 + MD);
	B5 = X1 + X2;
	T = (B5 + 8) / 16; //Temperature
	return T;
}

int32_t ION_BMP180_Pressure(bmp180Oss_t oss)
{
	BSP_I2C_Write_8(BMP180_I2C,BMP180_ADDRESS,BMP180_CONTROL,BMP180_READPRESSURECMD+(oss<<6));
	for(time = 0;time < 0xFFFF;time++); //wait?
	UP = (BSP_I2C_Read_16(BMP180_I2C,BMP180_ADDRESS,BMP180_PRESSUREDATA_MSB,BMP180_PRESSUREDATA_LSB)<<8)+(BSP_I2C_Read_8(BMP180_I2C,BMP180_ADDRESS,BMP180_PRESSUREDATA_XLSB)>>(8-oss));

	B6 = B5 - 4000;
	X1 = (B2 * (B6 * B6 / 4096)) / 2048;
	X2 = (AC2 * B6) / 2048;
	X3 = X1 + X2;
	B3 = (((AC1 * 4 + X3) << oss) + 2) / 4;
	X1 = (AC3 * B6) / 8192;
	X2 = (B1 * ((B6 * B6) / 4096))/65536;
	X3 = ((X1 + X2) + 2) / 4;
	B4 = AC4 * (uint32_t)(X3 + 32768) / 32768;
	B7 = (((uint32_t)UP) - B3) * (50000 >> oss);
	if(B7 < 0x80000000)
        P = (B7 * 2) / B4;
	else
        P = (B7 / B4) * 2;
	X1 = (P / 256) * (P / 256);
	X1 = (X1 * 3038) / 65536;
	X2 = ((-7357) * P) / 65536;
	P = P + (X1 + X2 + 3791) / 16; //Pressure
	return P;
}

bmp180_Temp_Pres ION_BMP180_Data(bmp180Oss_t oss)
{
    bmp180_Temp_Pres ION_BMP180_TP;
    ION_BMP180_CalibrationData();
    ION_BMP180_TP.BMP180_Data_T = ION_BMP180_Temperature(oss);
    ION_BMP180_TP.BMP180_Data_P = ION_BMP180_Pressure(oss);
    
    return ION_BMP180_TP;
}
