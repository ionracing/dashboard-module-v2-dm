/* Include guard ---------------------------------- */
#ifndef BSP_CAN_H
#define BSP_CAN_H

/* Includes ---------------------------------------- */
#include "BSP_GPIO.h"

#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

extern uint8_t precharge_done;
extern uint8_t BSPD_fault;
extern uint8_t IMD_fault;
extern uint8_t BMS_fault;
extern uint8_t TPOS_fault;

extern uint8_t cal_throttle_data;
extern uint8_t cal_break_data;

/* temporary IDs to test code */
#define CAN1_RX_BMS_FAULT 0x00
#define CAN1_RX_IMD_FAULT 0x01
#define CAN1_RX_BSPD_FAULT 0x02
#define CAN1_RX_TPOS_FAULT 0x03
#define CAN1_RX_PRECHARGE_DONE 0x04
#define CAN1_RX_TEMP 0x05
#define CAL_THROTTLE 0x06
#define CAL_BREAK 0x07

void BSP_CAN_Init(void);
void BSP_CAN_RCC_Init
(
    CAN_TypeDef* CANx
);
void BSP_CAN_GPIO_Init
(
    CAN_TypeDef* CANx
);
uint8_t BSP_CAN_Write_8
(
    CAN_TypeDef* CANx, 
    uint32_t address, 
    uint8_t length, 
    uint8_t data[8]
);
void BSP_CAN_Write_16
(
    CAN_TypeDef* CANx, 
    uint32_t address, 
    uint8_t length, 
    uint16_t data[8]
);
void CAN1_RX0_IRQHandler(void);
void CAN2_RX0_IRQHandler(void);

#endif //BSP_CAN_H
