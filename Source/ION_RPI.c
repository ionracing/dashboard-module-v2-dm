/**************************************************
*   RPI                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_RPI.h"

/**
 * We are useing a Raspberry Pi 2 model B and are communicating to this with USART.
 */
 
USART_TypeDef* RPI_USART = USART1;

void ION_RPI_Init()
{
    BSP_USART_Init(RPI_USART, 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx, USART_HardwareFlowControl_None);
}

void ION_RPI_Write(rpiMsg Data)
{
    BSP_USART_Write_8(RPI_USART, 0xFF);
    BSP_USART_Write_8(RPI_USART, 0xFF);
    BSP_USART_Write_8(RPI_USART, Data.id);
    BSP_USART_Write_8(RPI_USART, 0xFF & Data.data); //LSB
    BSP_USART_Write_8(RPI_USART, 0xFF & (Data.data >> 8)); //LMSB
    BSP_USART_Write_8(RPI_USART, 0xFF & (Data.data >> 16)); //MMSB
    BSP_USART_Write_8(RPI_USART, 0xFF & (Data.data >> 24)); //MSB
}

void ION_RPI_Write_String(uint8_t ID, char *chr)
{
    BSP_USART_Write_8(RPI_USART, 0xFF);
    BSP_USART_Write_8(RPI_USART, 0xFF);
    BSP_USART_Write_8(RPI_USART, ID);
    BSP_USART_Write_String(RPI_USART, chr);
}
