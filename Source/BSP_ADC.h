/* Include guard ---------------------------------- */
#ifndef BSP_ADC_H
#define BSP_ADC_H

/* Includes ---------------------------------------- */
#include "BSP_GPIO.h"

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_adc.h"

/* Function Prototypes ----------------------------- */
void BSP_ADC_init(void);
uint16_t BSP_getRawSingleChannel(void);
uint32_t BSP_ADC_getRawData(void);

/* Defines ----------------------------------------- */

/*
Channel mapping. The DMA arranges the sensor channels in 
wierd order. Therefore some defines are used to make it more
understandable.
*/
#define ADC_CH_0 = 6
#define ADC_CH_1 = 0
#define ADC_CH_2 = 2
#define ADC_CH_3 = 4
#define ADC_CH_4 = 1
#define ADC_CH_5 = 3
#define ADC_CH_6 = 5

/* Macros ----------------------------------------- */

#endif //BSP_ADC_H
