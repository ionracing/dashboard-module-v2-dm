/* Include guard ---------------------------------- */
#ifndef GLOBAL_H
#define GLOBAL_H

/* Includes ---------------------------------------- */
#include "ION_RPI.h"
#include "BSP_CAN.h"

#include "stm32f4xx.h"

extern uint8_t STARTUP_MODE;
extern uint8_t BLA_RIGHT;
extern uint8_t BLA_LEFT;
extern rpiMsg RPIringBuffer[500];
//extern CanRxMsg CANringBuffer[500];

#endif //GLOBAL_H
