/**************************************************
*   I2C                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_I2C.h"

/**
 * @brief Initialise for the I2Cx.
 *
 * @param I2Cx						x = 1, 2, 3.
 * @param Ack						I2C_Ack_x x => x = Enable, Disable.
 * @param AcknowledgedAddress		I2C_AcknowledgedAddress_x => x = 7bit, 10bit.
 * @param ClockSpeed				Standard 100KHz, check Manual.
 * @param DutyCycle					I2C_DutyCycle_x x => 2, 16_9.
 * @param Mode						I2C_Mode_x x => I2C, SMBusDevice, SMBusHost.
 * @param OwnAddress1				Master address not relevant.
 *
 */
void BSP_I2C_Init(I2C_TypeDef* I2Cx, uint16_t Ack, uint16_t AcknowledgedAdress, uint32_t ClockSpeed, uint16_t DutyCycle, uint16_t Mode, uint16_t OwnAddress1)
{
	I2C_InitTypeDef I2C_InitStruct;

	BSP_I2C_GPIO_Init(I2Cx);

	BSP_I2C_RCC_DeInit(I2Cx);
	BSP_I2C_RCC_Init(I2Cx);

	I2C_InitStruct.I2C_Ack = Ack;
	I2C_InitStruct.I2C_AcknowledgedAddress = AcknowledgedAdress;
	I2C_InitStruct.I2C_ClockSpeed = ClockSpeed;
	I2C_InitStruct.I2C_DutyCycle = DutyCycle;
	I2C_InitStruct.I2C_Mode = Mode;
	I2C_InitStruct.I2C_OwnAddress1 = OwnAddress1;
	I2C_Init(I2Cx,&I2C_InitStruct);

	I2C_Cmd(I2Cx, ENABLE);
}

/**
  * @brief Enables the right GPIOx too the right I2Cx.
  * @param I2Cx      x = 1, 2, 3.
  * 
  */
void BSP_I2C_GPIO_Init(I2C_TypeDef* I2Cx)
{
	if(I2Cx == I2C1)
	{
		BSP_GPIO_Init(GPIOB, GPIO_Mode_AF, GPIO_OType_OD, GPIO_Pin_6 | GPIO_Pin_9, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1); //SCL
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1); //SDA
	}
	else if(I2Cx == I2C2)
	{
		BSP_GPIO_Init(GPIOB, GPIO_Mode_AF, GPIO_OType_OD, GPIO_Pin_10 | GPIO_Pin_11, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_I2C2); //SCL
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_I2C2); //SDA
	}
	else if(I2Cx == I2C3)
	{
		BSP_GPIO_Init(GPIOA, GPIO_Mode_AF, GPIO_OType_OD, GPIO_Pin_8, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		BSP_GPIO_Init(GPIOC, GPIO_Mode_AF, GPIO_OType_OD, GPIO_Pin_9, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_I2C3); //SCL
		GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_I2C3); //SDA
	}
	else
		while(1);
}

/**
 * @brief Initialise I2Cx clock.
 *
 * @param I2Cx		x = 1, 2, 3.
 *
 */
void BSP_I2C_RCC_Init(I2C_TypeDef* I2Cx)
{
	if(I2Cx == I2C1)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);
	else if(I2Cx == I2C2)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2,ENABLE);
	else if(I2Cx == I2C3)
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C3,ENABLE);
	else
        while(1);
}

/**
 * @brief DeInitialise I2Cx clock.
 *
 * @param I2Cx		x = 1, 2, 3.
 *
 */
void BSP_I2C_RCC_DeInit(I2C_TypeDef* I2Cx)
{
	if(I2Cx == I2C1)
	{
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1,ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1,DISABLE);
	}
	else if(I2Cx == I2C2)
	{
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2,ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2,DISABLE);
	}
	else if(I2Cx == I2C3)
	{
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C3,ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C3,DISABLE);
	}
	else
        while(1);
}

/**
 * @brief Read 8bits from the I2Cx.
 *
 * @param I2Cx		x = 1, 2, 3.
 * @param addr		L3GD20H_ADDRESS, LSM303_ADDRESS, BMP180_ADDRESS or uint8_t value.
 * @param reg		L3GD20H__REGISTER_, LSM303_REGISTER_, BMP180_REGISTER_ or uint8_t value.
 * @param data		uint8_t value.
 *
 */
void BSP_I2C_Write_8(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg, uint8_t data)
{
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));

	I2C_Send7bitAddress(I2Cx, addr << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	I2C_SendData(I2Cx,reg);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	I2C_SendData(I2Cx, data);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
}

/**
 * @brief Read 8bits from the I2Cx.
 *
 * @param I2Cx		x = 1, 2, 3.
 * @param addr		L3GD20H_ADDRESS, LSM303_ADDRESS, BMP180_ADDRESS or uint8_t value.
 * @param reg		L3GD20H__REGISTER_, LSM303_REGISTER_, BMP180_REGISTER_ or uint8_t value.
 * @param data		uint8_t value.
 *
 */
void BSP_I2C_Write_16(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg, uint16_t data)
{
    BSP_I2C_Write_8(I2Cx, addr, reg, (uint8_t)data);
	BSP_I2C_Write_8(I2Cx, addr, reg, (uint8_t)(data >> 8));
}

/**
 * @brief Read 8bits from the I2Cx.
 *
 * @param I2Cx		x = 1, 2, 3.
 * @param addr		L3GD20H_ADDRESS, LSM303_ADDRESS, BMP180_ADDRESS or uint8_t value.
 * @param reg		L3GD20H__REGISTER_, LSM303_REGISTER_, BMP180_REGISTER_ or uint8_t value.
 *
 */
uint8_t BSP_I2C_Read_8(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg)
{
	uint8_t data = 0;

	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

	I2C_Send7bitAddress(I2Cx, addr << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	I2C_SendData(I2Cx, reg);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));

	I2C_Send7bitAddress(I2Cx, addr << 1, I2C_Direction_Receiver);
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

	I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
	I2C_AcknowledgeConfig(I2Cx, DISABLE);

    //not sure if this has been fixed or not.
	//need to add a wait here for new data, right now it goes to fast to get the new data.
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE));
	data = I2C_ReceiveData(I2Cx);
	//while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));

	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));

	return data;
}

/**
 * @brief Read 16bits from the I2Cx.
 *
 * @param I2Cx		x = 1, 2, 3.
 * @param addr		L3GD20H_ADDRESS, LSM303_ADDRESS, BMP180_ADDRESS or uint8_t value.
 * @param reg1		L3GD20H__REGISTER_, LSM303_REGISTER_, BMP180_REGISTER_ or uint8_t value.
 * @param reg2		L3GD20H__REGISTER_, LSM303_REGISTER_, BMP180_REGISTER_ or uint8_t value.
 *
 */
uint16_t BSP_I2C_Read_16(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg_MSB, uint8_t reg_LSB)
{
	uint8_t lsb = 0, msb = 0;
	uint16_t data = 0;

    lsb = BSP_I2C_Read_8(I2Cx, addr, reg_LSB);
	msb = BSP_I2C_Read_8(I2Cx, addr, reg_MSB);

	data = (msb << 8) | lsb;

	return data;
}

uint8_t BSP_I2C_Check(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg, uint8_t check)
{
    uint8_t mem = BSP_I2C_Read_8(I2Cx, addr, reg);
    if(mem == check)
        return 1;
    return 0;
}
