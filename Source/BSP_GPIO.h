/* Include guard ---------------------------------- */
#ifndef BSP_GPIO_H
#define BSP_GPIO_H

/* Includes ---------------------------------------- */
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void BSP_GPIO_Init
(
	GPIO_TypeDef* GPIOx,
	GPIOMode_TypeDef Mode,
	GPIOOType_TypeDef OType,
	uint16_t Pin,
	GPIOPuPd_TypeDef PuPd,
	GPIOSpeed_TypeDef Speed
);
void BSP_GPIO_RCC_Init
(
	GPIO_TypeDef* GPIOx
);
void BSP_GPIO_RCC_DeInit
(
	GPIO_TypeDef* I2Cx
);

#endif //BSP_HPIO_H
