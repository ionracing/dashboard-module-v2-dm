/*
* LEDs for debugging purposes.
* The LEDS are connected to:
*
* LED 1 : PD0
* LED 2 : PD1
* LED 3 : PD2
* LED 4 : PD3
*/

/* Includes --------------------------------------------------------*/
#include "BSP_LED.h"

/* Configuration ---------------------------------------------------*/
void BSP_LED_init(void)
{
	BSP_GPIO_Init(GPIOD, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15, GPIO_PuPd_NOPULL, GPIO_Speed_2MHz);
}

void BSP_LED_ON_RED()
{
    GPIO_SetBits(GPIOD, GPIO_Pin_12);
}

void BSP_LED_ON_GREEN()
{
    GPIO_SetBits(GPIOD, GPIO_Pin_13);
}

void BSP_LED_ON_ORANGE()
{
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
}

void BSP_LED_ON_BLUE()
{
    GPIO_SetBits(GPIOD, GPIO_Pin_15);
}

void BSP_LED_OFF_RED()
{
    GPIO_ResetBits(GPIOD, GPIO_Pin_12);
}

void BSP_LED_OFF_GREEN()
{
    GPIO_ResetBits(GPIOD, GPIO_Pin_13);
}

void BSP_LED_OFF_ORANGE()
{
    GPIO_ResetBits(GPIOD, GPIO_Pin_14);
}

void BSP_LED_OFF_BLUE()
{
    GPIO_ResetBits(GPIOD, GPIO_Pin_15);
}

void BSP_LED_TOGGLE_RED()
{
    GPIO_ToggleBits(GPIOD, GPIO_Pin_12);
}

void BSP_LED_TOGGLE_GREEN()
{
    GPIO_ToggleBits(GPIOD, GPIO_Pin_13);
}

void BSP_LED_TOGGLE_ORANGE()
{
    GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
}

void BSP_LED_TOGGLE_BLUE()
{
    GPIO_ToggleBits(GPIOD, GPIO_Pin_15);
}

void BSP_LED_ON_ALL()
{
    BSP_LED_ON_RED();
    BSP_LED_ON_GREEN();
    BSP_LED_ON_ORANGE();
    BSP_LED_ON_BLUE();
}

void BSP_LED_OFF_ALL()
{
    BSP_LED_OFF_RED();
    BSP_LED_OFF_GREEN();
    BSP_LED_OFF_ORANGE();
    BSP_LED_OFF_BLUE();
}

void BSP_LED_TOGGLE_ALL()
{
    BSP_LED_TOGGLE_RED();
    BSP_LED_TOGGLE_GREEN();
    BSP_LED_TOGGLE_ORANGE();
    BSP_LED_TOGGLE_BLUE();
}
