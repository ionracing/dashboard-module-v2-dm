/* Include guard ---------------------------------- */
#ifndef BSP_IC_H
#define BSP_IC_H

/* Includes ---------------------------------------- */
#include "stm32f4xx_tim.h"
#include "stm32f4xx_exti.h"

void BSP_IC_init(void);
void TIM8_CC_IRQHandler(void);

#endif //BSP_IC_H
