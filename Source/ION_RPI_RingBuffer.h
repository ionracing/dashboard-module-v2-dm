/* Include guard ---------------------------------- */
#ifndef ION_RPI_RINGBUFFER_H
#define ION_RPI_RINGBUFFER_H

/* Includes ---------------------------------------- */
#include "ION_RPI.h"

#include "stm32f4xx.h"

void ION_RPI_RingBuffer_Init(rpiMsg* array, int length);
void ION_RPI_RingBuffer_Add(rpiMsg Add);
rpiMsg ION_RPI_RingBuffer_Get(void);
int ION_RPI_RingBuffer_hasValue(void);

#endif //ION_RPI_RINGBUFFER_H
