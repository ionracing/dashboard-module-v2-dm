/* Include guard ---------------------------------- */
#ifndef ION_RPI_H
#define ION_RPI_H

/* Includes ---------------------------------------- */
#include "BSP_USART.h"

#include "stm32f4xx.h"

typedef enum
{
    L3GD20H_Axis_X          = 0xC8,
    L3GD20H_Axis_Y          = 0xC9,
    L3GD20H_Axis_Z          = 0xCA,
    LSM303_Accel_X          = 0xCB,
    LSM303_Accel_Y          = 0xCC,
    LSM303_Accel_Z          = 0xCD,
    LSM303_Mag_X            = 0xCE,
    LSM303_Mag_Y            = 0xCF,
    LSM303_Mag_Z            = 0xD0,
    BMP180_Temp             = 0xD1,
    BMP180_Pres             = 0xD2,
    GPS_ID                  = 0xD3
} USART_ID;

typedef struct
{
    uint8_t id;
    uint32_t data;
} rpiMsg;

void ION_RPI_Init(void);

void ION_RPI_Write
(
    rpiMsg Data
);
void ION_RPI_Write_String
(
    uint8_t ID, 
    char *chr
);

#endif //ION_RPI_H
