/**************************************************
*   L3GD20H                                       *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_L3GD20H.h"

I2C_TypeDef* L3GD20H_I2C = I2C2;

/**
 * @brief Initialise for the L3GD20H.
 *
 */
uint8_t _ION_L3GD20H_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    L3GD20H_I2C = I2Cx;
    
	//Power ON
    BSP_I2C_Write_8(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, 0x00); //Reset
	BSP_I2C_Write_8(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, L3GD20H_AXIS_AND_POWER_ON);
    if(!BSP_I2C_Check(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, L3GD20H_AXIS_AND_POWER_ON))
        return 0;
    
    BSP_I2C_Write_8(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG4, L3GD20H_RANGE_250DPS);
    if(!BSP_I2C_Check(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG4, L3GD20H_RANGE_250DPS))
        return 0;
    
    return 1;
}

/**
 * @brief Initialise for the L3GD20H.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t ION_L3GD20H_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    L3GD20H_I2C = I2Cx;
	BSP_I2C_Init(I2Cx,I2C_Ack_Enable,I2C_AcknowledgedAddress_7bit, 100000, I2C_DutyCycle_2, I2C_Mode_I2C, 0x00);

	//Power ON
    BSP_I2C_Write_8(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, 0x00); //Reset
	BSP_I2C_Write_8(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, L3GD20H_AXIS_AND_POWER_ON);
    if(!BSP_I2C_Check(L3GD20H_I2C, L3GD20H_ADDRESS, L3GD20H_REGISTER_CTRL_REG1, L3GD20H_AXIS_AND_POWER_ON))
        return 0;
    
    return 1;
}

/**
 * @brief Get X axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_L3GD20H_X()
{
	return BSP_I2C_Read_16(L3GD20H_I2C,L3GD20H_ADDRESS,L3GD20H_REGISTER_OUT_X_H,L3GD20H_REGISTER_OUT_X_L);
}

/**
 * @brief Get Y axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_L3GD20H_Y()
{
	return BSP_I2C_Read_16(L3GD20H_I2C,L3GD20H_ADDRESS,L3GD20H_REGISTER_OUT_Y_H,L3GD20H_REGISTER_OUT_Y_L);
}

/**
 * @brief Get X axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_L3GD20H_Z()
{
	return BSP_I2C_Read_16(L3GD20H_I2C,L3GD20H_ADDRESS,L3GD20H_REGISTER_OUT_Z_H,L3GD20H_REGISTER_OUT_Z_L);
}

/**
 * @brief Get X, Y and Z axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 * Returns a struct.
 */
l3gd20hAxis ION_L3GD20H_XYZ()
{
	l3gd20hAxis AXIS;
	AXIS.X = ION_L3GD20H_X();
	AXIS.Y = ION_L3GD20H_Y();
	AXIS.Z = ION_L3GD20H_Z();
	return AXIS;
}
