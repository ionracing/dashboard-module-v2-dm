/* Include guard ---------------------------------- */
#ifndef ION_10_DOF_IUM_H
#define ION_10_DOF_IUM_H

/* Includes ---------------------------------------- */
#include "BSP_I2C.h"
#include "ION_L3GD20H.h"
#include "ION_LSM303.h"
#include "ION_BMP180.h"
#include "ION_RPI_RingBuffer.h"

void ION_TEN_DOF_IUM_Init(void);
void ION_TEN_DOF_IUM_GetL3GD20H(void);
void ION_TEN_DOF_IUM_GetLSM303(void);
void ION_TEN_DOF_IUM_GetBMP180(void);

#endif //ION_10_DOF_IUM_H
