/**************************************************
*   RingBuffer                                    *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_CAN_RingBuffer.h"

/*********************INITS*********************/
int ION_CAN_Head, ION_CAN_Tail, ION_CAN_Length;
long ION_CAN_readCounter, ION_CAN_writeCounter;
CanRxMsg* ION_CAN_Start;

void ION_CAN_RingBuffer_Init(CanRxMsg* start, int length)
{
    ION_CAN_Head = 0;
    ION_CAN_Tail = 0;
    ION_CAN_readCounter = 0;
    ION_CAN_writeCounter = 0;
    ION_CAN_Start = start;
    ION_CAN_Length = length;
}

void ION_CAN_RingBuffer_Add(CanRxMsg Add)
{
    if((ION_CAN_writeCounter + 1) - ION_CAN_readCounter >= ION_CAN_Length)
        ION_CAN_Tail++;
    ION_CAN_Head++;
    ION_CAN_writeCounter++;
    if(ION_CAN_Head == ION_CAN_Length)
        ION_CAN_Head = 0;
    *(ION_CAN_Start + ION_CAN_Head) = Add;
}

CanRxMsg ION_CAN_RingBuffer_Get()
{
    ION_CAN_readCounter++;
    if(ION_CAN_Tail == ION_CAN_Length)
        ION_CAN_Tail = 0;
    return *(ION_CAN_Start + ION_CAN_Tail++);
}

int ION_CAN_RingBuffer_hasValue()
{
    if(ION_CAN_Head == ION_CAN_Tail)
        return 0;
    else
        return 1;
}
