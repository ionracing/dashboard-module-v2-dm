/**************************************************
*   IRQHANDLERS                                   *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_IRQHandler.h"

uint8_t status = 0xEE;
CanTxMsg canMsg;
uint8_t preAmbl = 0x00;
uint8_t idLsb = 0;
uint16_t idCan = 0;

void USART3_IRQHandler(void)
{
    uint8_t data;
	// check if the USART3 receive interrupt flag was set
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
        data = USART_ReceiveData(USART3);
        
        if (status < 2) //add ID check for error handeling
        {
            if(status == 0)
            {
                idLsb = data;
            }
            else
            {
                idCan = (data << 8) | idLsb;
                if(idCan < 700 || idCan >= 800) //Wrong ID send error to RPI.
                {
                    USART_SendData(USART3, 0x20);
                    status = 0xEE;
                }
                canMsg.StdId = idCan;
                canMsg.IDE = CAN_Id_Standard;
                canMsg.RTR = CAN_RTR_Data;
                canMsg.DLC = 4;
            }
            status++;
        }
        else if (status >= 2 && status < 6) // Add the 4 bytes of data to the CAN data frame.
        {
            canMsg.Data[status-2] = data;
            status++;
        }
        else if (status == 6) // Send to RPI usaing CAN1.
        {
            CAN_Transmit(CAN1, &canMsg);
            status++;
        }
        else
        {
            if(data == 0xFF)
            {
                if(preAmbl == 0xFF)
                {
                    status = 0;
                    preAmbl = 0x00;
                }
                else
                {
                    preAmbl = data;
                }
            }
        }
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
	}
}

uint8_t RxByte = 0;
/*
void USART1_IRQHandler(void){
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
	// check if the USART1 receive interrupt flag was set
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART1,USART_IT_RXNE);
        USART_Read(USART1);
	}
}
*/
void USART2_IRQHandler(void){
	// check if the USART2 receive interrupt flag was set
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		BSP_USART_Read(USART2);
        USART_ClearITPendingBit(USART2,USART_IT_RXNE);
	}
}

CanRxMsg msgRx;

void CAN1_RX0_IRQHandler (void)
{
    rpiMsg CANtoRPI;
    uint8_t LEN;
    
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
		CAN_Receive(CAN1, CAN_FIFO0, &msgRx); //receiveing message on can1, First In First Out filter
        
        if(msgRx.StdId == 0x265) // StartupmodeON/driveModeOff.
        {
            STARTUP_MODE = 1;
        }
        else if(msgRx.StdId == 0x26E) // StartupmodeOFF/DriveModeON
        {
            STARTUP_MODE = 0;
        }
        else
        {
            //ION_CAN_RingBuffer_Add(msgRx);
            
            CANtoRPI.id = msgRx.StdId;
            if((CANtoRPI.id >= 0xC8 && CANtoRPI.id <= 0xD3) || CANtoRPI.id == 0x85)//check for used id's
            {
                CANtoRPI.id = 0x66; //arbitrary id value.
            }
            for(LEN = 0;LEN < 4;LEN++)
            {
                CANtoRPI.data = msgRx.Data[LEN] << (8 * LEN);
                if((LEN+1) == msgRx.DLC)
                {
                    break;
                }
            }
            ION_RPI_RingBuffer_Add(CANtoRPI);
        }
	}
	__enable_irq();
    
    /*
    
    if(CAN1->RF0R & CAN_RF0R_FMP0)
    { 					 //checks if a message is pending
		CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);    //receiveing message on can1, First In First Out filter
		if(rxmsg.StdId == 0x210)
        {
            GPIO_SetBits(GPIOB, GPIO_Pin_11);
        }

		if(*rxmsg.Data == 0x0A)
		{
			GPIO_ToggleBits(GPIOA, GPIO_Pin_7);
		}	
		else if(*rxmsg.Data == 0x0D)
        {
			GPIO_ToggleBits(GPIOA, GPIO_Pin_5);
		} 
		
		if(rxmsg.StdId == 0x100) 
		{
			rpm = (int16_t) ((rxmsg.Data[1]<<8) | rxmsg.Data[0]); 
			rpm = rpm*8;
			if(rpm < 100)
			{
				rpm = 0;
			}
			rpmRequest[0] = 0x31;  //rmp in the motorcontroller 
			rpmRequest[1] = 0xFF & rpm;  //by using "&" we remove all unused bits 
			rpmRequest[2] = rpm >> 8;
			
			BSP_CAN2_Tx(0x210, 3, rpmRequest);

            //rpm = (int16_t) ((rxmsg.Data[1]<<8) | rxmsg.Data[0]); 
            //rpm = rpm*8;

			//rpmRequest[0] = 0x77;  //rmp in the motorcontroller 
            //rpmRequest[1] = 0xF;
			//rpmRequest[2] = rpm >> 8;
            
            //BSP_CAN2_Tx(0x210, 2, rpmRequest);
            
            //value = rxmsg.Data[0] | (rxmsg.Data[1] << 8);
	
		
            //CAN1 on the RPI module must receive all sensor data. 
            //the BMS fault, IMD fault, BSPD fault, Throttle Pos Sensor fault
            //precharge signal OK and stuff
		
            //Error msgs from CAN1 bus  
            //maybe poll them in check_errors() function? 
			//so the TIM function that is to be called every 
			// half a second or so polling the BMS IMD ... faults 
		
            //put big data in a buffer
            //otherwise just set a flag
		
		
            if(calib_request == 1) //if the user will be calibrating receive these informations
            {
                // calib MIN & calib MAX success for every sensor 
			
                switch(rxmsg.StdId)
                {
                    case(CAL_THROTTLE):
                        //do stuff here
                    
                        break;
                    case(CAL_BREAK):
                        //do stuff here
                    
                        break;
                    
                    //and so on...
                }
            }
		
		if(rxmsg.StdId == CAN1_RX_TEMP)
        {
            //data goes in temperature buffer
		}
		
		if(rxmsg.StdId == CAN1_RX_PRECHARGE_DONE)
        {
            precharge_done = 1;
		} 
		
		if(rxmsg.StdId == CAN1_RX_BMS_FAULT)
        {
			BMS_fault = 1;
		}

		if (rxmsg.StdId == CAN1_RX_IMD_FAULT)
        {
			IMD_fault = 1;
		}

		if (rxmsg.StdId == CAN1_RX_BSPD_FAULT)
        {
			BSPD_fault = 1;
		} 

		if (rxmsg.StdId == CAN1_RX_TPOS_FAULT)
        {
				TPOS_fault = 1;
		}
		// Sensor data interrupt from CAN1 bus
		// only receive data here, process data elsewhere
	}
    */
}

void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        /* Do your stuff when PD0 is changed */
        STARTUP_MODE = STARTUP_MODE == 0 ? 1 : 0;
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}

void CAN2_RX0_IRQHandler (void)
{
	__disable_irq();
	if(CAN2->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
		CAN_Receive(CAN2, CAN_FIFO0, &msgRx);
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        //Add msgRx to a buffer and prosess it another place.
        if(*msgRx.Data == 0xFF)
        {
            GPIO_SetBits(GPIOD, GPIO_Pin_14);
        }
	}
	__enable_irq();
}
