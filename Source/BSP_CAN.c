/**************************************************
*   CAN                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_CAN.h"

CAN_TypeDef* CANx = CAN1;

/* 
* RPI Comm board & ECU board & STM32F4-Discovery board
* PB8 : CAN1 RX  
* PB9 : CAN1 TX
*
* ECU board
* PB5 : CAN2 RX
* PB6 : CAN2 TX
*/

void BSP_CAN_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;
	CAN_InitTypeDef CAN_InitStruct;
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
    // Have to set the can tranceiver EN pin high to enable it
	GPIO_SetBits(GPIOB, GPIO_Pin_6); //CAN1 Enable on RPI board
    
	BSP_CAN_GPIO_Init(CANx);
	
	BSP_CAN_RCC_Init(CANx);
	
	/* CAN register init */
	CAN_DeInit(CANx);
	
	/* CAN cell init */
	CAN_InitStruct.CAN_TTCM = DISABLE;
	CAN_InitStruct.CAN_ABOM = DISABLE;
	CAN_InitStruct.CAN_AWUM = DISABLE;
	CAN_InitStruct.CAN_NART = DISABLE;
	CAN_InitStruct.CAN_RFLM = DISABLE;
	CAN_InitStruct.CAN_TXFP = DISABLE;
	CAN_InitStruct.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStruct.CAN_SJW = CAN_SJW_3tq;

    //Bus length 3-5m.
    
    
	/* CAN Baudrate = 500 KBps (CAN clocked at 42 MHz) */
	CAN_InitStruct.CAN_BS1 = CAN_BS1_4tq;
	CAN_InitStruct.CAN_BS2 = CAN_BS2_2tq;
	CAN_InitStruct.CAN_Prescaler = (42000000 / 7) / 500000; // -> 12
	CAN_Init(CANx, &CAN_InitStruct);

	/* CAN filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = CANx == CAN1 ? 0 : 14;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);

	/* Enable CAN1 RX0 interrupt IRQ channel */
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

void BSP_CAN_GPIO_Init(CAN_TypeDef * CANx)
{
	if(CANx == CAN1)
	{
		BSP_GPIO_Init(GPIOB,GPIO_Mode_AF,GPIO_OType_PP,GPIO_Pin_8 | GPIO_Pin_9,GPIO_PuPd_UP,GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); //RX
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); //TX
	}
	else if(CANx == CAN2)
	{
		BSP_GPIO_Init(GPIOB,GPIO_Mode_AF,GPIO_OType_PP,GPIO_Pin_5 | GPIO_Pin_6,GPIO_PuPd_UP,GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_CAN2); //RX
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_CAN2); //TX
	}
}

/**
 *
 *
 */
void BSP_CAN_RCC_Init(CAN_TypeDef * CANx)
{
    if (CANx == CAN1)
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1,ENABLE);
    else if (CANx == CAN2)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1,ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2,ENABLE);
    }
    else
      while(1);
}

// CAN Transmit
uint8_t i = 0;

uint8_t BSP_CAN_Write_8(CAN_TypeDef* CANx, uint32_t address, uint8_t length, uint8_t data[8])
{
    CanTxMsg msg;
	msg.StdId 	= address;
	msg.IDE 	= CAN_Id_Standard;
	msg.RTR		= CAN_RTR_Data;
	msg.DLC		= length;
    
	for(i=0; i < length; i++)
    {
		msg.Data[i] = data[i];
	}
	return CAN_Transmit(CANx, &msg);
}

// CAN Transmit 16bit.
uint8_t j = 0;
uint8_t data_LSB[8];
uint8_t data_MSB[8];

void BSP_CAN_Write_16(CAN_TypeDef* CANx, uint32_t address, uint8_t length, uint16_t data[8])
{
    for(j = 0;j < length;j++)
    {
        data_LSB[j] = (uint8_t)(data[j]);
        data_MSB[j] = (uint8_t)(data[j] >> 8);
    }
    BSP_CAN_Write_8(CANx, address, length, data_LSB);
    BSP_CAN_Write_8(CANx, address, length, data_MSB);
}

