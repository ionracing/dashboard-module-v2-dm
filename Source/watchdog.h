/* Include guard ---------------------------------- */
#ifndef WATCHDOG_H
#define WATCHDOG_H

/* Includes ---------------------------------------- */
#include "stm32f4xx.h"


void watchdog_init(void);
void watchdog_pet(void);

#endif //WATCHDOG_H
