/* Include guard ---------------------------------- */
#ifndef BSP_USART_H
#define BSP_USART_H

/* Includes ---------------------------------------- */
#include "BSP_GPIO.h"
#include "BSP_CAN.h"

#include "stm32f4xx_usart.h"
#include "stm32f4xx.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "misc.h"

extern char USARTData[100];
extern volatile uint16_t GPS_New_Data;

void BSP_USART_Init
(
	USART_TypeDef* USARTx,
	uint32_t BaudRate,
	uint16_t WordLength,
	uint16_t StopBits,
	uint16_t Parity,
	uint16_t Mode,
	uint16_t HardwareFlowControl
);
void BSP_USART_GPIO_Init
(
	USART_TypeDef* USARTx
);
void BSP_USART_RCC_Init
(
	USART_TypeDef* USARTx
);
void BSP_USART_NVIC_Init
(
    USART_TypeDef* USARTx
);
void BSP_USART_Write_8
(
    USART_TypeDef* USARTx, 
    uint16_t Data
);
void BSP_USART_Write_16
(
    USART_TypeDef* USARTx, 
    uint16_t Data
);
void BSP_USART_Write_String
(
    USART_TypeDef* USARTx, 
    char *chr
);
void BSP_USART_Read
(
		USART_TypeDef* USARTx
);

#endif //BSP_USART_H
