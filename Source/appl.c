/* Includes */
#include "ION_RPI.h"
#include "ION_RPI_RingBuffer.h"
#include "ION_CAN_RingBuffer.h"
#include "BSP_systick.h"
#include "BSP_Interrupt.h"
#include "BSP_Startup.h"
#include "BSP_CAN.h"
#include "ION_L3GD20H.h"
#include "ION_LSM303.h"
#include "ION_BMP180.h"
#include "ION_10-DOF_IUM.h"
#include "ION_GPS.h"
#include "BSP_systick.h"
#include "BSP_USART.h"
#include "global.h"
#include "stm32f4xx_tim.h"
#include "BSP_LED.h"

#define RPI_SIZE 1000
//#define CAN_SIZE 100
    
uint8_t BLA_RIGHT = 0;
uint8_t BLA_LEFT = 0;
uint8_t STARTUP_MODE = 1;
    
int main(void)
{
    rpiMsg BLA;
    rpiMsg RPIringBuffer[RPI_SIZE];
    
    //CanRxMsg CANringBuffer[CAN_SIZE];
    //CanRxMsg canMsg;
    //rpiMsg canToRpi;
    //uint8_t len;
    
    BLA.id = 0x85;
    
    //Init
    ION_RPI_RingBuffer_Init(RPIringBuffer, RPI_SIZE);
    //ION_CAN_RingBuffer_Init(CANringBuffer, CAN_SIZE);
    
    BSP_Interrupt_init();
	BSP_CAN_Init();
    
    //ION_TEN_DOF_IUM_Init();
    ION_GPS_Init();
    ION_RPI_Init();
    BSP_systick_init();
    //BSP_LED_init();
	
    while(1)
    {
        //BSP_LED_ON_ALL();
        if(STARTUP_MODE)
        {
            if(GPS_New_Data)
            {
                GPS_New_Data = 0;
                ION_RPI_Write_String(GPS_ID, USARTData); //gpsData.
            }
            /**
            if(ION_CAN_RingBuffer_hasValue())
            {
                canMsg = ION_CAN_RingBuffer_Get();
                
                //do what you want with the data.
                if(canMsg.StdId == 0xFFFF) // change this to what you desire.
                {
                    
                }
                else //Sending everything to rpi.
                {
                    canToRpi.id = canMsg.StdId;
                    if(canToRpi.id >= 0xC8 && canToRpi.id <= 0xD3)
                    {
                        canToRpi.id = 0x66;
                    }
                    for(len = 0;len < 4;len++)
                    {
                        canToRpi.data = canMsg.Data[len] << (8 * len);
                        if((len+1) == canMsg.DLC)
                        {
                            break;
                        }
                    }
                    ION_RPI_RingBuffer_Add(canToRpi);
                }
            }**/
            if(BLA_RIGHT)
            {
                BLA.data = 1;
                ION_RPI_Write(BLA);
                BLA_RIGHT = 0;
            }
            if(BLA_LEFT)
            {
                BLA.data = 0;
                ION_RPI_Write(BLA);
                BLA_LEFT = 0;
            }
        }
        else
        {
            if(GPS_New_Data)
            {
                GPS_New_Data = 0;
                ION_RPI_Write_String(GPS_ID, USARTData); //gpsData.
            }
            
            if(clk10ms == CLK_COMPLETE)
            {
                clk10ms = CLK_RESET;
                ION_TEN_DOF_IUM_GetL3GD20H();
                ION_TEN_DOF_IUM_GetLSM303();
            }
            
            if(clk1000ms == CLK_COMPLETE)
            {
                clk1000ms = CLK_RESET;
                ION_TEN_DOF_IUM_GetBMP180();
            }
            
            if(ION_RPI_RingBuffer_hasValue())
            {
                ION_RPI_Write(ION_RPI_RingBuffer_Get());
            }
            /**
            if(ION_CAN_RingBuffer_hasValue())
            {
                canMsg = ION_CAN_RingBuffer_Get();
                
                //do what you want with the data.
                if(canMsg.StdId == 0xFFFF) // change this to what you desire.
                {
                    
                }
                else //Sending everything to rpi.
                {
                    canToRpi.id = canMsg.StdId;
                    if(canToRpi.id >= 0xC8 && canToRpi.id <= 0xD3)
                    {
                        canToRpi.id = 0x66;
                    }
                    for(len = 0;len < 4;len++)
                    {
                        canToRpi.data = canMsg.Data[len] << (8 * len);
                        if((len+1) == canMsg.DLC)
                        {
                            break;
                        }
                    }
                    ION_RPI_RingBuffer_Add(canToRpi);
                }
            }**/
        }
	}
}
