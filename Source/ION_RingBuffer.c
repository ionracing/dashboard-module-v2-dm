/**************************************************
*   RingBuffer                                    *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_RingBuffer.h"

/*********************INITS*********************/
int Head, Tail, Length;
long readCounter, writeCounter;
rpiMsg* Start;

void ION_RingBuffer_Init(rpiMsg* start, int length)
{
    Head = 0;
    Tail = 0;
    Start = start;
    Length = length;
}

void ION_RingBuffer_Add(rpiMsg Add)
{
    if((writeCounter + 1) - readCounter >= Length)
        Tail++;
    Head++;
    writeCounter++;
    if(Head == Length)
        Head = 0;
    *(Start + Head) = Add;
}

rpiMsg ION_RingBuffer_Get()
{
    readCounter++;
    if(Tail == Length)
        Tail = 0;
    return *(Start + Tail++);
}

int ION_RingBuffer_hasValue()
{
    if(Head == Tail)
        return 0;
    else
        return 1;
}
