/**
@brief input capture. Used for computing the frequency of the
rotation of the vehicles wheel.

It uses the special timers tim1 and tim8 (ch1, ch2, ch3, ch4).
----------------------------------------------------------------
wheel sensor 1	:	PC7		:	TIM8 CH2
wheel sensor 2  : PC8		: TIM8_CH3
wheel sensor 3  : PE9		: TIM1_CH1
wheel sensor 4  : PE11	: TIM1_CH2
----------------------------------------------------------------
@Inputs : uint8_t Channel selects the wheel sensor channel.
					can be either 1, 2, 3 or 4.
----------------------------------------------------------------					
FIRST TEST ON PC7 TIM8_CH2!
----------------------------------------------------------------
*/
// TODO : Possible to change to a 32 bit counter and still use the input capture?

/* includes ---------------------------------------------------*/
#include "BSP_IC.h"

/* Macros -----------------------------------------------------*/
#define OVERFLOW (w1_readValue2 < w1_readValue1)

/* Function prototypes ----------------------------------------*/
static void BSP_IC_GPIO_init(void);
static void BSP_IC_NVIC_init(void);
static void BSP_IC_TIM_init(void);

/* Global variables -------------------------------------------*/
/*wn = wheel sensor n*/
uint16_t w1_readValue1 = 0;
uint16_t w1_readValue2 = 0;
uint16_t w1_captureNumber = 0;
uint32_t w1_capture = 0;
uint32_t w1_Freq = 0;
 
 
/* Init function --------------------------------------------- */
void BSP_IC_init()
{
	/* Enable Timer clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
	/* Configure GPIO as Timer input */
	BSP_IC_GPIO_init();
	/* Configure and enable the NVIC */
	BSP_IC_NVIC_init();
	/* Configure the timer*/
	BSP_IC_TIM_init();
}

//********************************************************************************
// Local init functions
//********************************************************************************

/*
GPIO init for the frequency counter.
*/
static void BSP_IC_GPIO_init(void)
{
	GPIO_InitTypeDef GPIO_initStruct;
	
		/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	/* Configure the GPIO inputs*/
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Pin = GPIO_Pin_7;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	
	GPIO_Init(GPIOC, &GPIO_initStruct);
	
	/* Configure the GPIO pins as timer inputs*/
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM8);
}
/*
 Configures the tim time base and tim special features. 
*/
static void BSP_IC_TIM_init()
{
	TIM_TimeBaseInitTypeDef TIM_initStruct;
	TIM_ICInitTypeDef TIM_ICinitStruct;
	
	/* Configure the time base unit */
	// TODO : configure the base init for the correct frequency.
	TIM_initStruct.TIM_Period = 0xFFFF;
	TIM_initStruct.TIM_Prescaler = 0x000;
	TIM_initStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_initStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_initStruct.TIM_RepetitionCounter = 0x0000;
	
    TIM_TimeBaseInit(TIM8, &TIM_initStruct);
  
	/* Configure the channel to measure frequency */
	TIM_ICinitStruct.TIM_Channel = TIM_Channel_2;
	TIM_ICinitStruct.TIM_ICFilter = 0x0; // 0x0 -> 0xFF
	TIM_ICinitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICinitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1; // Capture performed each time an edge is detected on the capture input.
	TIM_ICinitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	
	TIM_ICInit(TIM8, &TIM_ICinitStruct);
	
	/* Enable the counter */
	TIM_Cmd(TIM8, ENABLE);
}


/* GPIO must be initialised for NVIC prior to calling this function. */
static void BSP_IC_NVIC_init()
{
	
	NVIC_InitTypeDef NVIC_initStruct;
	/* Select the input pin for the EXTI-Line */
	NVIC_initStruct.NVIC_IRQChannel = TIM8_CC_IRQn;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_initStruct);
	/* Enable the interrupt to read the captured value. */
	TIM_ITConfig(TIM8, TIM_IT_CC2, ENABLE);
}


/* IRQ HANDLER ---------------------------------------------------*/

// TODO : Change functions to macros.

/* TODO : 
check the counter for overflow and set the frequency
to zero if the two counter overflows occour two times 
before the second capture has occoured. Intended 
to set the frequency to zero if the rotation of the 
wheel is to low.
*/

/* 
For every rising flank the timer latches the value. This IRQ handler
computes the period between two latched values (i.e two rising pulses)
and computes the frequency.
*/
#define readCapturedData TIM8->CCR2

void TIM8_CC_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM8, TIM_IT_CC2) == SET)
	{
		TIM_ClearITPendingBit(TIM8, TIM_IT_CC2);
		if(w1_captureNumber == 0)
		{
			w1_readValue1 = readCapturedData;//TIM_GetCapture2(TIM8);
			w1_captureNumber = 1;
		}
		else if(w1_captureNumber == 1)
		{
			w1_readValue2 = TIM_GetCapture2(TIM8);
			if (!OVERFLOW)
				w1_capture = (w1_readValue2 - w1_readValue1);
			else if(OVERFLOW)
				w1_capture = ((0xFFFF - w1_readValue1) + w1_readValue2);
			else
				w1_capture = 0;
			/* Calculate the frequency */
			w1_Freq = (uint32_t) SystemCoreClock / w1_capture;
			w1_captureNumber = 0;
		}
	}
}

uint32_t BSP_IC_getFreq(uint8_t channel)
{
	switch(channel)
	{
		case(1):
			return w1_Freq;
		case(2):
			break; // return w2_Freq;
		case(3):
			break; // return w3_Freq;
		case(4):
			break; // return w4_Freq
		default:
			return 0;
	}
  return 0;
}

/*
void TIM1_CC_IRQHandler(void)
{
	
}
*/
