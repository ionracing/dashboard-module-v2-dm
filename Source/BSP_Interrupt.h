/* Include guard ---------------------------------- */
#ifndef BSP_INTERRUPT_H
#define BSP_INTERRUPT_H

/* Includes ---------------------------------------- */
#include "BSP_CAN.h"
#include "BSP_Startup.h"
#include "BSP_GPIO.h"

#include "misc.h"
#include "stdint.h"
#include "stm32f4xx.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"

extern uint8_t START_BUTTON;
extern uint8_t STOP_BUTTON;
extern uint8_t PDM_comparator;

void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void BSP_Interrupt_init(void);
void EXTI15_10_IRQHandler(void);

#endif //BSP_INTERRUPT_H
