/**************************************************
*   ADC                                           *
*   Configures the ADC channels                   *
*   for the sensor module.                        *
*                                                 *
*   TODO: Add oversamplin. Board specific?        *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_ADC.h"

/*********************DEFINES**********************/

/**
* @Adefgroup ADC: Define the channels to be used. Comment out channels that
									are unused.
*/
#define ADC_GPIO = GPIOA
#define ADC_CH0 ADC_Channel_0
#define ADC_CH1 ADC_Channel_1
#define ADC_CH2 ADC_Channel_2
#define ADC_CH3 ADC_Channel_3
#define ADC_CH4 ADC_Channel_4
#define ADC_CH5 ADC_Channel_5
#define ADC_CH6 ADC_Channel_6
#define ADC_CH7 ADC_Channel_7

#define ADC_ALL_CHANNELS (ADC_CH0 | ADC_CH1 | ADC_CH2 | ADC_CH3 | ADC_CH4 | ADC_CH5 | ADC_CH6)
#define ADC_SAMPLING_FREQUENCY 1000 //Hz

/**
* @defgroup DMA
*/
#define ADC_DMA_BUFFERSIZE 8

/* Local function prototypes ----------------------------------*/
/*static void BSP_ADC_initDMA(void);
static void BSP_ADC_initGPIO(GPIO_TypeDef* GPIOx, uint16_t GPIO_pin);
static void BSP_ADC_initTIM(uint16_t frequency);
static void BSP_ADC_initADC(void);
static void BSP_ADC_initNVIC(void);
static void BSP_ADC_initChannel(void);*/
//static void BSP_ADC_initchannel(uint16_t channel, uint16_t sampling_frequency);

/********************VARIABLES*********************/
__IO uint32_t ADC_convertedValues = 0;

/* Init functions -------------------------------------------- */
void BSP_ADC_init(void)
{
	
	ADC_InitTypeDef 			ADC_initStruct;
	ADC_CommonInitTypeDef ADC_commonInitStruct;
	DMA_InitTypeDef 			DMA_initStruct;
	GPIO_InitTypeDef			GPIO_InitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	 /* DMA2 Stream0 channel2 configuration **************************************/
	DMA_initStruct.DMA_Channel = DMA_Channel_2;
	DMA_initStruct.DMA_PeripheralBaseAddr = (uint32_t)0x40012308;
	DMA_initStruct.DMA_Memory0BaseAddr = (uint32_t)&ADC_convertedValues;
	DMA_initStruct.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_initStruct.DMA_BufferSize = 1;
	DMA_initStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_initStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_initStruct.DMA_Mode = DMA_Mode_Circular;
	DMA_initStruct.DMA_Priority = DMA_Priority_High;
	DMA_initStruct.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_initStruct.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_initStruct.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_initStruct.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0, &DMA_initStruct);
	DMA_Cmd(DMA2_Stream0, ENABLE);
	
	/* Configure ADC1 Channel0 pin as analog input ******************************/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	/* ADC Common Init **********************************************************/
	ADC_commonInitStruct.ADC_Mode = ADC_Mode_Independent;
	ADC_commonInitStruct.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_commonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);
	
	/* ADC1 Init ****************************************************************/
	ADC_initStruct.ADC_Resolution = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode = DISABLE;
	ADC_initStruct.ADC_ContinuousConvMode = ENABLE;
	ADC_initStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConvEdge_None;
	ADC_initStruct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_initStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_initStruct);
	
	/* ADC3 regular channel7 configuration **************************************/
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0,1,ADC_SampleTime_144Cycles);
	
	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
	
	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);
	
	/* Enable ADC3 */
	ADC_Cmd(ADC1, ENABLE);
	
	ADC_SoftwareStartConv(ADC1);
}

uint32_t BSP_ADC_getRawData(void)
{
	ADC_convertedValues;
    return 0xFFFFFFFF; //fix this later
}
