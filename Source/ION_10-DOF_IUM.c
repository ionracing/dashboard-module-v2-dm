/**************************************************
*   10-DOF_IUM                                    *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_10-DOF_IUM.h"

I2C_TypeDef* I2Cx = I2C1;
rpiMsg RPIMsg;


/**
 * @brief Initialise the 10-DOF_IUM.
 *
 * @param I2Cx		x = 1, 2, 3.
 *
 */
void ION_TEN_DOF_IUM_Init()
{
	//I2C
	BSP_I2C_Init(I2Cx,I2C_Ack_Enable,I2C_AcknowledgedAddress_7bit, 100000, I2C_DutyCycle_2, I2C_Mode_I2C, 0x00);

    //Periph
	_ION_L3GD20H_Init(I2Cx);
    _ION_LSM303_ACCEL_Init(I2Cx);
    _ION_LSM303_MAG_Init(I2Cx);
    _ION_BMP180_Init(I2Cx);
}

void ION_TEN_DOF_IUM_GetL3GD20H()
{
    l3gd20hAxis L3GAxis;
    
    L3GAxis = ION_L3GD20H_XYZ(); //Axis
    
    RPIMsg.id = L3GD20H_Axis_X;
    RPIMsg.data = L3GAxis.X;
    
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = L3GD20H_Axis_Y;
    RPIMsg.data = L3GAxis.Y;
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = L3GD20H_Axis_Z;
    RPIMsg.data = L3GAxis.Z;
    ION_RPI_RingBuffer_Add(RPIMsg);
}

void ION_TEN_DOF_IUM_GetLSM303()
{
    lsm303AccelAxis LSMAccelAxis;
    lsm303MagAxis LSMMagAxis;
    
    LSMAccelAxis = ION_LSM303_ACCEL_XYZ(); //Acceleration
    RPIMsg.id = LSM303_Accel_X;
    RPIMsg.data = LSMAccelAxis.X;
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = LSM303_Accel_Y;
    RPIMsg.data = LSMAccelAxis.Y;
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = LSM303_Accel_Z;
    RPIMsg.data = LSMAccelAxis.Z;
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    LSMMagAxis = ION_LSM303_MAG_XYZ(); //Magnetometer
    RPIMsg.id = LSM303_Mag_X;
    RPIMsg.data = LSMMagAxis.X;

    ION_RPI_RingBuffer_Add(RPIMsg);
 
    RPIMsg.id = LSM303_Mag_Y;
    RPIMsg.data = LSMMagAxis.Y;
    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = LSM303_Mag_Z;
    RPIMsg.data = LSMMagAxis.Z;
    ION_RPI_RingBuffer_Add(RPIMsg);
}

void ION_TEN_DOF_IUM_GetBMP180()
{
    bmp180_Temp_Pres BMP180_TP;
    
    BMP180_TP = ION_BMP180_Data(BMP180_OSS_SINGLE);
    
    RPIMsg.id = BMP180_Temp;
    RPIMsg.data = (uint32_t)BMP180_TP.BMP180_Data_T;

    ION_RPI_RingBuffer_Add(RPIMsg);
    
    RPIMsg.id = BMP180_Pres;
    RPIMsg.data = (uint32_t)BMP180_TP.BMP180_Data_P;
    ION_RPI_RingBuffer_Add(RPIMsg);
}
