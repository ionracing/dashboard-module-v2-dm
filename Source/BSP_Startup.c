/**************************************************
*   STARTUP                                       *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_Startup.h"

CanRxMsg can1rx;
//maybe check_rrors should also check temperature from battery and state of charge and such?
// if everything is OK then set an flag = 1 
// and after the user has chosen to calibrate or not calibrate sensors, another flag is set
// 

//Test variables.
uint8_t calib_Request;
uint8_t usart_ID;
uint8_t BMS_fault;
uint8_t BSPD_fault;
uint8_t IMD_fault;
uint8_t TPOS_fault;
uint8_t cal_throttle;
uint8_t precharge_done;
uint8_t cal_throttle_data;

uint8_t fault_check = 0;
uint8_t calibrating_done = 0;


void BSP_Startup_startup_routine(void)
{

    if (clk5000ms == CLK_COMPLETE) //wait 5 seconds before moving on in the startuproutine
    {							   //because this will be called right after the vehicle has gotten power, 
                                   //so we expect to have no data before a few seconds
        BSP_Startup_check_errors();
        clk5000ms = CLK_RESET;
    }
		
		/* calibrating_done = 1    when calibrating is done
	     fault_check      = 1    when there are no errors sent
			 precharge_done   = 1    when precharge_done signal received
			
		   So, if not all of these are 1, u are stuck in the loop untill everything is OK
		*/
		
    while( calibrating_done==0 && fault_check==0 && precharge_done==0 )
    {  
        //if calibrating is done, user will not enter this loop again. just waiting for safety checks.
        while(calibrating_done==0)
        {
            //stuck in this loop untill the user is finished calibrating
            //then set calibrating_done = 1
            //receive data from USART1 which sensor is to be calibrated
                
            //this must be received over USART1 from raspberry pi
                    
            switch(calib_Request) //this variable is found in BSP_USART.c because user sends data from display to RPI comm board
            {
                case '1':
                    //calibrate(); //this function will include calibration for all sensors and stuff
                    break;
                case '0':
                    //user has pressed a button that says calibrating is done (whether or not calibrating actually took place)
                    //calibrating_done();  //a function that sends to RPI that calibrating is finished. (so in the display the calibrating graphic will dissapear or one can just scroll to another screen)
                    calibrating_done = 1;
                    break;
            }
        }	
        //check once again if everything is alright.
        if (clk5000ms == CLK_COMPLETE)			
        {																	
            BSP_Startup_check_errors();
            clk5000ms = CLK_RESET;
        }
    }
}

void calibrate(void)
{
	while(calib_Request == 1) //stay in this while loop untillthe user presses a button on the display to signal that calibrating is done
    {
		uint8_t throttle_MAX = 0;
		uint8_t throttle_MIN = 0;
		uint8_t break_MAX = 0;
		uint8_t break_MIN = 0;
	
		//this switch will handle all the different sensors to calibrate
		switch(usart_ID) //where usart_id is the users input. for example if the user wants to calibrate throttle position sensor. then that switch case will match usart_id
        { 
			case'1': //brake pedal ID
				while(throttle_MAX == 0 && throttle_MIN == 0) //stuck in this while loop untill user says calibration is OK for brake MAX and brake MIN
                {
                    // "calibration successfull" will be received over CAN bus from the sensor modules
                    if(cal_throttle_data == 0)
                    {
                        //calibrate max position for the sensor.
					}
                    else if(cal_throttle_data == 2)
                    {
                        //calibrate minimum position for the sensor. 
					}
					if(cal_throttle_data == 1)
                        throttle_MAX = 1;
					if(cal_throttle_data == 3)
                        throttle_MIN = 1;
                }
				break;
			case'2': //throttle pedal ID 
				while(break_MAX == 0 && break_MIN == 0)
                {
                    //process received data from CAN1 bus
				}
			break;
		}
	}
}
// this function will also be used in the TIMER interrupt thingy, which will interrupt every second or so. continous safety checks

// or maybe CAN1 IRQ Handler puts all data in to a buffer and the Check_errors process that data?
void BSP_Startup_check_errors(void)
{
	//CAN1RX interrupt will set flags accordingly 
    if(precharge_done == 1)
    { 
        //send a message?
        //maybe a message define that is PRECHARGE and if data = 0, then precharge isnt done. and data = 1, precharge is done?
    }
    else if(BMS_fault == 1)
    {
        fault_check++;
    }
    else if (IMD_fault == 1)
    {
        fault_check++;
    }
    else if (BSPD_fault == 1)
    {
        fault_check++;
    }
    else if (TPOS_fault == 1)
    {
        fault_check++;
    }
}
