/* Include guard ---------------------------------- */
#ifndef ION_CAN_RINGBUFFER_H
#define ION_CAN_RINGBUFFER_H

/* Includes ---------------------------------------- */
#include "BSP_CAN.h"

#include "stm32f4xx.h"

void ION_CAN_RingBuffer_Init(CanRxMsg* array, int length);
void ION_CAN_RingBuffer_Add(CanRxMsg Add);
CanRxMsg ION_CAN_RingBuffer_Get(void);
int ION_CAN_RingBuffer_hasValue(void);

#endif //ION_CAN_RINGBUFFER_H
