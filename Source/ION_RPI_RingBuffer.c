/**************************************************
*   RingBuffer                                    *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_RPI_RingBuffer.h"

/*********************INITS*********************/
int ION_RPI_Head, ION_RPI_Tail, ION_RPI_Length;
long ION_RPI_readCounter, ION_RPI_writeCounter;
rpiMsg* ION_RPI_Start;

void ION_RPI_RingBuffer_Init(rpiMsg* start, int length)
{
    ION_RPI_Head = 0;
    ION_RPI_Tail = 0;
    ION_RPI_Start = start;
    ION_RPI_Length = length;
}

void ION_RPI_RingBuffer_Add(rpiMsg Add)
{
    if((ION_RPI_writeCounter + 1) - ION_RPI_readCounter >= ION_RPI_Length)
        ION_RPI_Tail++;
    ION_RPI_Head++;
    ION_RPI_writeCounter++;
    if(ION_RPI_Head == ION_RPI_Length)
        ION_RPI_Head = 0;
    *(ION_RPI_Start + ION_RPI_Head) = Add;
}

rpiMsg ION_RPI_RingBuffer_Get()
{
    ION_RPI_readCounter++;
    if(ION_RPI_Tail == ION_RPI_Length)
        ION_RPI_Tail = 0;
    return *(ION_RPI_Start + ION_RPI_Tail++);
}

int ION_RPI_RingBuffer_hasValue()
{
    if(ION_RPI_Head == ION_RPI_Tail)
        return 0;
    else
        return 1;
}
