#include "stm32f4xx.h"
#include "ION_RPI.h"

void ION_RingBuffer_Init(rpiMsg* array, int length);
void ION_RingBuffer_Add(rpiMsg Add);
rpiMsg ION_RingBuffer_Get(void);
int ION_RingBuffer_hasValue(void);
