/* Include guard ---------------------------------- */
#include "BSP_GPIO.h"

void BSP_LED_init(void);
void BSP_LED_ON_RED(void);
void BSP_LED_ON_GREEN(void);
void BSP_LED_ON_ORANGE(void);
void BSP_LED_ON_BLUE(void);
void BSP_LED_OFF_RED(void);
void BSP_LED_OFF_GREEN(void);
void BSP_LED_OFF_ORANGE(void);
void BSP_LED_OFF_BLUE(void);
void BSP_LED_TOGGLE_RED(void);
void BSP_LED_TOGGLE_GREEN(void);
void BSP_LED_TOGGLE_ORANGE(void);
void BSP_LED_TOGGLE_BLUE(void);
void BSP_LED_ON_ALL(void);
void BSP_LED_OFF_ALL(void);
void BSP_LED_TOGGLE_ALL(void);
