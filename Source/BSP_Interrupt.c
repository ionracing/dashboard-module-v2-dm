/**************************************************
*   INTERRUPTS                                    *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "BSP_Interrupt.h"
#include "global.h"

#define BSP_CAN_TX_START_BUTTON 0xAA   //random IDs to test cantx code
#define BSP_CAN_TX_STOP_BUTTON 0x0E

uint8_t data[1] = {1};  //an array of length 1 with the value 1 (logic high)
												//because the CAN_TX function takes in an array of max length, 1 byte
uint8_t PDM_comparator = 0;

/*
* PC8  : Cooling ON/OFF
* PC9  : Cooling regulation ON/OFF
* PC12 : START button
* PC13 : STOP button
*/

uint8_t STOP_BUTTON = 99;
uint8_t START_BUTTON = 99;


void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void BSP_Interrupt_init(void);

void BSP_Interrupt_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_12 | GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	
	
//	GPIO_Initialise(GPIOC, GPIO_Mode_IN, GPIO_OType_PP, GPIO_Pin_12, GPIO_PuPd_DOWN, GPIO_Speed_100MHz);

	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8); //Tell system PC8 is EXTI_Line8;
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource9); //Tell system PC9 is EXTI_Line9;
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource12); //Tell system PC12 is EXTI_Line12;
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13); //Tell system PC13 is EXTI_Line13;


	EXTI_InitStruct.EXTI_Line = EXTI_Line8 | EXTI_Line9 | EXTI_Line12 | EXTI_Line13; 
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn; //pins from 5 - 9
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn; //pins from 10 - 15
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	
}

// __disable_irq(); and __enable_irq(); stops further interrupts from happening whilst in this handler. then activates it again

	//need both interrupts in this handler because PC5 - PC9 uses EXTI9_5_IRQHandler and we're using PC8 and PC9
void EXTI9_5_IRQHandler(void)
{
    __disable_irq();
	//PC8 interrupt Cooling ON/OFF
	if(EXTI_GetITStatus(EXTI_Line8) != RESET) //this line checks which flag is set. I.e. which button is pressed.
    {
        //GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
		BLA_RIGHT = 1;
		EXTI_ClearITPendingBit(EXTI_Line8);  // this will clear the interrupt on the EXTI line
	}
    
	//PC9 interrupt Cooling regulation ON/OFF
	if(EXTI_GetITStatus(EXTI_Line9) != RESET)
    {
        //GPIO_ToggleBits(GPIOD,GPIO_Pin_15);
		BLA_LEFT = 1;
		EXTI_ClearITPendingBit(EXTI_Line9);
	}
	__enable_irq();
}
	
	
	//PC10 - PC15 uses this handler. so have to handle both interrupts in this handler
void EXTI15_10_IRQHandler(void)
{ 
	__disable_irq();
	//PC12 interrupt START button
	if(EXTI_GetITStatus(EXTI_Line12) != RESET)
    {
        GPIO_ToggleBits(GPIOB, GPIO_Pin_5); //transistor controlling output on the RPI board
        GPIO_ToggleBits(GPIOC, GPIO_Pin_4); //LED2 on the RPI comm board
	
        // here we send data over CAN bus that start button has been pressed
        BSP_CAN_Write_8(CAN1, BSP_CAN_TX_START_BUTTON, 1, data);
		
		//	STOP_BUTTON = 0;
		//	START_BUTTON = 1;
		
        EXTI_ClearITPendingBit(EXTI_Line12); 
	}
    //PC13 interrupt STOP button
	if(EXTI_GetITStatus(EXTI_Line13) != RESET)
    {
			
        //GPIO_ToggleBits(GPIOD, GPIO_Pin_15);
		BSP_CAN_Write_8(CAN1, BSP_CAN_TX_STOP_BUTTON, 1, data);  //data is the same as in START BUTTON because it is only a logic high = 1.
													// just to signify that we have pressed the button
		calibrating_done = 0;  
		fault_check = 0;
		STOP_BUTTON = 1;
		START_BUTTON = 0;
		EXTI_ClearITPendingBit(EXTI_Line13);
	}
	__enable_irq();
}
