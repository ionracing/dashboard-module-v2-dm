/**************************************************
*   LSM303                                        *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "ION_LSM303.h"

I2C_TypeDef* LSM303_I2C = I2C1;

/**
 * @brief Initialise for the LSM303 Accelerometer.
 * 
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t _ION_LSM303_ACCEL_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    LSM303_I2C = I2Cx;
    
    //Power ON
	BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x57);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x57))
        return 0;
        
    BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG4_A, 0x10);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG4_A, 0x10))
        return 0;
    
    return 1;
}

/**
 * @brief Initialise for the LSM303 Accelerometer.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t ION_LSM303_ACCEL_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    LSM303_I2C = I2Cx;
    BSP_I2C_Init(LSM303_I2C,I2C_Ack_Enable,I2C_AcknowledgedAddress_7bit, 100000, I2C_DutyCycle_2, I2C_Mode_I2C, 0x00);
    
    //Power ON
	BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x57);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x57))
        return 0;
        
    BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG4_A, 0x40);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_CTRL_REG4_A, 0x40))
        return 0;
    
    return 1;
}

/**
 * @brief Initialise for the LSM303 Magnetometer.
 * 
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t _ION_LSM303_MAG_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    LSM303_I2C = I2Cx;
    
    //Power ON
	BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRA_REG_M, 0x94);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRA_REG_M, 0x94))
        return 0;
        
    BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_MR_REG_M, 0x00);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_MR_REG_M, 0x00))
        return 0;
   
    setMagGain(LSM303_MAGGAIN_1_3);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRB_REG_M, LSM303_MAGGAIN_1_3))//+-1.3G gain.
        return 0;
    
    return 1;
}

/**
 * @brief Initialise for the LSM303 Magnetometer.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint8_t ION_LSM303_MAG_Init(I2C_TypeDef* I2Cx)
{
    //I2C
    LSM303_I2C = I2Cx;
	BSP_I2C_Init(I2Cx,I2C_Ack_Enable,I2C_AcknowledgedAddress_7bit, 100000, I2C_DutyCycle_2, I2C_Mode_I2C, 0x00);

	//Power ON
	BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRA_REG_M, 0x94);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRA_REG_M, 0x94))
        return 0;
        
    BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_MR_REG_M, 0x00);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_MR_REG_M, 0x00))
        return 0;
    
    setMagGain(LSM303_MAGGAIN_1_3);
    if(!BSP_I2C_Check(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRB_REG_M, LSM303_MAGGAIN_1_3))//+-1.3G gain.
        return 0;
    
    return 1;
}

float _lsm303Mag_Gauss_LSB_XY = 1100.0F;  // Varies with gain
float _lsm303Mag_Gauss_LSB_Z  = 980.0F;   // Varies with gain
void setMagGain(lsm303MagGain gain)
{
    BSP_I2C_Write_8(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_CRB_REG_M, (uint8_t)gain);

    switch(gain)
    {
        case LSM303_MAGGAIN_1_3:
            _lsm303Mag_Gauss_LSB_XY = 1100;
            _lsm303Mag_Gauss_LSB_Z  = 980;
            break;
        case LSM303_MAGGAIN_1_9:
            _lsm303Mag_Gauss_LSB_XY = 855;
            _lsm303Mag_Gauss_LSB_Z  = 760;
            break;
        case LSM303_MAGGAIN_2_5:
            _lsm303Mag_Gauss_LSB_XY = 670;
            _lsm303Mag_Gauss_LSB_Z  = 600;
            break;
        case LSM303_MAGGAIN_4_0:
            _lsm303Mag_Gauss_LSB_XY = 450;
            _lsm303Mag_Gauss_LSB_Z  = 400;
            break;
        case LSM303_MAGGAIN_4_7:
            _lsm303Mag_Gauss_LSB_XY = 400;
            _lsm303Mag_Gauss_LSB_Z  = 355;
            break;
        case LSM303_MAGGAIN_5_6:
            _lsm303Mag_Gauss_LSB_XY = 330;
            _lsm303Mag_Gauss_LSB_Z  = 295;
            break;
        case LSM303_MAGGAIN_8_1:
            _lsm303Mag_Gauss_LSB_XY = 230;
            _lsm303Mag_Gauss_LSB_Z  = 205;
            break;
    }
}

/**
 * @brief Get Accelerometer X axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_ACCEL_X()
{
	return (BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_OUT_X_H_A, LSM303_REGISTER_ACCEL_OUT_X_L_A));// * 0.001F * 9.80665F);
}

/**
 * @brief Get Accelerometer Y axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_ACCEL_Y()
{
	return (BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_OUT_Y_H_A, LSM303_REGISTER_ACCEL_OUT_Y_L_A));// * 0.001F * 9.80665F);
}

/**
 * @brief Get Accelerometer Z axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_ACCEL_Z()
{
	return (BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_ACCEL, LSM303_REGISTER_ACCEL_OUT_Z_H_A, LSM303_REGISTER_ACCEL_OUT_Z_L_A));// * 0.001F * 9.80665F);
}

/**
 * @brief Get Accelerometer X, Y and Z axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
lsm303AccelAxis ION_LSM303_ACCEL_XYZ()
{
	lsm303AccelAxis AXIS;
	AXIS.X = ION_LSM303_ACCEL_X();
	AXIS.Y = ION_LSM303_ACCEL_Y();
	AXIS.Z = ION_LSM303_ACCEL_Z();
	return AXIS;
}

/**
 * @brief Get Magnetometer X axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_MAG_X()
{
	return ((BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_OUT_X_H_M, LSM303_REGISTER_MAG_OUT_X_L_M)));//  / _lsm303Mag_Gauss_LSB_XY) * 100);
}

/**
 * @brief Get Magnetometer Y axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_MAG_Y()
{
	return ((BSP_I2C_Read_16(LSM303_I2C,LSM303_ADDRESS_MAG,LSM303_REGISTER_MAG_OUT_Y_H_M,LSM303_REGISTER_MAG_OUT_Y_L_M)));//  / _lsm303Mag_Gauss_LSB_XY) * 100);
}

/**
 * @brief Get Magnetometer Z axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 *
 */
uint16_t ION_LSM303_MAG_Z()
{
	return ((BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_OUT_Z_H_M,LSM303_REGISTER_MAG_OUT_Z_L_M)));// / _lsm303Mag_Gauss_LSB_Z) * 100);
}

/**
 * @brief Get Magnetometer X, Y and Z axis.
 *
 * @param I2Cx			x = 1, 2, 3.
 */
lsm303MagAxis ION_LSM303_MAG_XYZ()
{
	lsm303MagAxis AXIS;
	AXIS.X = ION_LSM303_MAG_X();
	AXIS.Y = ION_LSM303_MAG_Y();
	AXIS.Z = ION_LSM303_MAG_Z();
	return AXIS;
}

/**
 * @brief Get Temperature.
 *
 */
uint16_t ION_LSM303_TEMP()
{
	return BSP_I2C_Read_16(LSM303_I2C, LSM303_ADDRESS_MAG, LSM303_REGISTER_MAG_TEMP_OUT_H_M,LSM303_REGISTER_MAG_TEMP_OUT_L_M);
}
