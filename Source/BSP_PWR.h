/* Include guard ---------------------------------- */
#ifndef BSP_PWR_H
#define BSP_PWR_H

/* Includes ---------------------------------------- */
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void BSPD_PWR_init(void);
void BSDP_PWR_analogON(void);
void BSPD_PWR_analogOff(void);
uint16_t BSPD_PWR_checkErrorState(void);

#endif //BSP_PWR_H
